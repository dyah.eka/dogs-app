const DOG_API_PLURAL_URL = 'https://dog.ceo/api/breeds'
const DOG_API_SINGULAR_URL = 'https://dog.ceo/api/breed'

export const state = () => ({
  dog_loading: false,
  dog_breeds: {},
  dog_subBreeds: [],
})

export const getters = {
  dog_loading: (state) => state.dog_loading,
  dog_breeds: (state) => state.dog_breeds,
  dog_subBreeds: (state) => state.dog_subBreeds,
}

export const mutations = {
  dog_SET_LOADING: (state, payload) => {
    state.dog_loading = payload
  },
  dog_SET_BREEDS: (state, payload) => {
    state.dog_breeds = payload
  },
  dog_SET_SUB_BREEDS: (state, payload) => {
    state.dog_subBreeds = payload
  },
  dog_CLEAR_SUB_BREEDS: (state, payload) => {
    state.dog_subBreeds = []
  },
}

export const actions = {
  /**
   * @description Fetch List All Breed
   *
   * @method GET
   * @access public
   *
   * @return {Promise<any>} Promise<any>
   */
  dog_fetchBreedList: async function ({ commit }) {
    commit('dog_SET_LOADING', true)

    const controller = new AbortController()
    const timeout = setTimeout(() => {
      if (!controller.signal.aborted) controller.abort()
    }, 5000)

    try {
      const response = await fetch(`${DOG_API_PLURAL_URL}/list/all`, {
        signal: controller.signal,
      })
      const { message } = await response.json()

      commit('dog_SET_BREEDS', message)

      return Promise.resolve(message)
    } catch (error) {
      return Promise.reject(error)
    } finally {
      commit('dog_SET_LOADING', false)
      clearTimeout(timeout)
    }
  },

  /**
   * @description Fetch List Sub Breed
   *
   * @method GET
   * @access public
   *
   * @return {Promise<any>} Promise<any>
   */
  dog_fetchSubBreedList: async function ({ commit }, { breed }) {
    commit('dog_SET_LOADING', true)

    const controller = new AbortController()
    const timeout = setTimeout(() => {
      if (!controller.signal.aborted) controller.abort()
    }, 10000)

    try {
      const response = await fetch(`${DOG_API_SINGULAR_URL}/${breed}/list`, {
        signal: controller.signal,
      })
      const { message } = await response.json()

      commit('dog_SET_SUB_BREEDS', message)

      return Promise.resolve(message)
    } catch (error) {
      return Promise.reject(error)
    } finally {
      commit('dog_SET_LOADING', false)
      clearTimeout(timeout)
    }
  },

  /**
   * @description Fetch Image Url of Dog Breed
   *
   * @method GET
   * @access public
   *
   * @return {Promise<any>} Promise<any>
   */
  dog_fetchBreedImageUrl: async function (_, { breed, numberOfImages }) {
    try {
      const response = await fetch(
        `${DOG_API_SINGULAR_URL}/${breed}/images/random${
          numberOfImages ? `/${numberOfImages}` : ''
        }`
      )
      const { message } = await response.json()

      return Promise.resolve(message)
    } catch (error) {
      return Promise.reject(error)
    }
  },

  /**
   * @description Fetch Image Url of Dog Sub Breed
   *
   * @method GET
   * @access public
   *
   * @return {Promise<any>} Promise<any>
   */
  dog_fetchSubBreedImageUrl: async function (
    _,
    { breed, subBreed, numberOfImages }
  ) {
    try {
      const response = await fetch(
        `${DOG_API_SINGULAR_URL}/${breed}/${subBreed}/images/random${
          numberOfImages ? `/${numberOfImages}` : ''
        }`
      )
      const { message } = await response.json()

      return Promise.resolve(message)
    } catch (error) {
      return Promise.reject(error)
    }
  },
}
